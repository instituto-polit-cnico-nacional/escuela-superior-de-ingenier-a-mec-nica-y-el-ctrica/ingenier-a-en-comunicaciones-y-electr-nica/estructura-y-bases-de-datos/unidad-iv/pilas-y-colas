# UNIDAD I: Pilas y Colas.

## OBJETIVOS PARTICULARES DE LA UNIDAD

El alumno resolverá problemas de ingeniería y ciencias, utilizando programas que simulen el comportamiento de una pila o de una cola.

| No. Tema  | Título Tema                         | Teoría              | Práctica            | Evaluación Continua | Clave Bibliográfica  |
| :---      | :----                               | :---:               | :---:               | :---:              | :---:                |
| 4.1       | PILAS                               | :white_check_mark:  | :white_check_mark:  | :white_check_mark: | [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf) |
| 4.1.1     | Concepto de Pila.                   | :white_check_mark:  | :x:                 | :x: |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf) |
| 4.1.2     | Definición Clase pila               | :white_check_mark:  | :x:                 | :x:              |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf)              |
| 4.1.2.1   | Arreglos                          | :white_check_mark:    | :x:                 | :x:                |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf) |
| 4.1.2.2   | Lista enlazada, (STL)              | :x:                  | :x:                 | :white_check_mark: |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf)            |
| 4.1.2.3   | Ejemplo de Aplicación              | : :x:                | :white_check_mark: | :white_check_mark: |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf)              |
| 4.1.3     | Implementación con POO              | :x:                 | :x:                 |:white_check_mark:               |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf)              |
| 4.2       | COLAS                               | :white_check_mark:  |:white_check_mark:  | :white_check_mark:  |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf)|
| 4.2.1     | Concepto de Cola.                    | :white_check_mark: | :x:                | :x: |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf) |
| 4.2.2     | Definición Clase Cola                | :white_check_mark: | :x:                | :x:  |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf) |
| 4.2.2.1   | Arreglos                             | :white_check_mark: | :x:                 | :x:                |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf) |
| 4.2.2.2   | Lista enlazada, (STL)                | :x:                | :x:                 | :white_check_mark:       |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf) |
| 4.2.2.3   | Ejemplo de Aplicación                | :x:                | :white_check_mark:  | :white_check_mark: |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf) |
| 4.2.3     | Implementación con POO               | :x:                | :x:                 | :white_check_mark: |  [2B,3B,6C,7C](activos/pdfs/UNIDAD_I_-_Representación_de_bajo_y_alto_nivel_de_datos.pdf) |
|           | Horas Totales                        | 8.0                | 3.0                 | 8.0                |              |

## ESTRATEGIA DIDÁCTICA

Resolución de ejercicios aplicando pilas y colas coordinado por el profesor.
Utilizando un programa para solución de problemas de ingeniería empleando pilas y /o colas

## PROCEDIMIENTO DE EVALUACIÓN

* Programas y ejercicios desarrollados en clase y extra clase.
* Examen del periodo.
